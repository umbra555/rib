const HEART_BEAT_INTERVAL = 2000;

let heartBeat = setInterval(heart, HEART_BEAT_INTERVAL);

const process = [
    require('./tasks/task_01'),
    require('./tasks/task_02'),
];

let curTask = 0;

function heart() {
    if (curTask == process.length) {
        console.log("exec complite!");
        clearInterval(heartBeat);
        return;
    }

    let task = process[curTask];

    console.log(task.getStatus());

    if (task.status == 0) {
        task.status = 1;
        console.log(`Текущий шаг: ${task.name}`);
        task.proc(task);
    }

    if (task.status == 2) {
        curTask++;
    }
}