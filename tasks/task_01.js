const Task = require('./task');

function execTask() {
    console.log("Hi, i'am task #1");

    setTimeout(() => {
        this.status = 2;
    }, 5000);
}

module.exports = new Task(
    "Task #1",
    execTask,
)