const status = {
    0: 'ready to run',
    1: 'running',
    2: 'complite'
}

class Task {
    constructor(name, proc) {
        this.name = name;
        this.proc = proc;
        this.result = "";
        this.status = 0;
    }

    getStatus() {
        return status[this.status]
    }

    setStatus(val) {
        this.status = val;
    }

}

module.exports = Task